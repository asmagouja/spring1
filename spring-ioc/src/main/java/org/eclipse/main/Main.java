package org.eclipse.main;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.eclipse.nation.European;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class Main 
{
    public static void main( String[] args ) {
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    Personne p = context.getBean("per",Personne.class);
    Adresse adresse = context.getBean("adresse",Adresse.class);
    p.afficher(); 
    adresse.afficher();
    
    European e = (European) context.getBean("eng");
    European e1 = (European) context.getBean("french");
    e.saluer();
    e1.saluer();
    }
}

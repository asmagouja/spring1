package org.eclipse.spring.formulaire.controller;

import java.util.List;

import org.eclipse.spring.formulaire.dao.PersonneRepository;
import org.eclipse.spring.formulaire.model.Personne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.context.request.WebRequest;

@Controller
public class ConnextionController {

	@Autowired
	private PersonneRepository personneRepository;

	@GetMapping("/connect")
	public String personneForm(Model model) {
		model.addAttribute("perso", new Personne());
		return "connectForm";
	}

	@PostMapping("/connect")
	public String checkData(@ModelAttribute("perso") Personne personne, BindingResult result, Model model,
			WebRequest request) {
		List<Personne> personnes = personneRepository.findByNomAndPrenom(personne.getNom(), personne.getPrenom());
		if (personnes.size() > 0) {
			request.setAttribute("connected", true, WebRequest.SCOPE_SESSION);//ajoute l'attribut(l'information) connected=true dans la session
			request.setAttribute("perso", personne, WebRequest.SCOPE_SESSION);//Maintenant qu'on a v�rifi� que la personne existe en base, on va ajouter l'attribut(l'information) perso=personne dans la session
			System.out.println("PERSO / " + request.getAttribute("perso",WebRequest.SCOPE_SESSION));//Pour v�rifier si perso existe dans la session
			return "redirect:personne";//Rediriger la request vers le FormController (GET /personne)
		}
		return "connectForm";
	}

	@GetMapping("/disconnect")
	public String leave(WebRequest request) {
		request.setAttribute("connected", false, WebRequest.SCOPE_SESSION);
		request.removeAttribute("perso", WebRequest.SCOPE_SESSION);
		return "redirect:connect";//Redirect la request vers ConnexionController (GET /connect)
	}

}
